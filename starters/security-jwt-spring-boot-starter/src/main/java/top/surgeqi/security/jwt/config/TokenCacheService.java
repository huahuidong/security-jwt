package top.surgeqi.security.jwt.config;

/**
 * <p><em>Created by qipp on 2020/10/6 10:15 上午</em></p>
 * 令牌缓存操作类
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public interface TokenCacheService {

    /**
     * 缓存标志位
     */
    String FLAG = "T";

    /**
     * 存储登出令牌与刷新令牌
     *
     * @param accessToken  令牌
     * @param refreshToken 刷新令牌
     * @author qipp
     */
    void storeLogoutToken(String accessToken, String refreshToken);

    /**
     * 令牌或刷新令牌是否已登出
     *
     * @param token 令牌或刷新令牌
     * @return boolean
     * @author qipp
     */
    boolean isLogoutToken(String token);
}
