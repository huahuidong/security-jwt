package top.surgeqi.security.jwt.config;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.core.EhcacheManager;
import org.springframework.util.StringUtils;

/**
 * <p><em>Created by qipp on 2020/10/6 10:54 上午</em></p>
 * Ehcache 令牌缓存操作类
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class TokenEhCacheServiceImpl implements TokenCacheService {

    /**
     * 缓存操作类
     */
    private final EhcacheManager cacheManager;
    /**
     * JWT相关配置
     */
    private final JwtProperties jwtProperties;

    public TokenEhCacheServiceImpl(EhcacheManager cacheManager, JwtProperties jwtProperties) {
        this.cacheManager = cacheManager;
        this.jwtProperties = jwtProperties;
    }

    @Override
    public void storeLogoutToken(String accessToken, String refreshToken) {
        if (!StringUtils.isEmpty(accessToken)) {
            Cache<String, String> cache = cacheManager.getCache(jwtProperties.getLogoutAccessTokenName(), String.class, String.class);
            cache.put(accessToken, FLAG);
            log.info("用户登出令牌->{}",accessToken);
        }
        if (!StringUtils.isEmpty(refreshToken)) {
            Cache<String, String> refreshTokenCache = cacheManager.getCache(jwtProperties.getLogoutRefreshTokenName(), String.class, String.class);
            refreshTokenCache.put(refreshToken, FLAG);
            log.info("用户登出刷新令牌->{}",refreshToken);
        }
    }

    @Override
    public boolean isLogoutToken(String token) {
        log.info("判断令牌是否被用户登出->{}",token);
        if (!StringUtils.isEmpty(token)) {
            Cache<String, String> cache = cacheManager.getCache(jwtProperties.getLogoutAccessTokenName(), String.class, String.class);
            Cache<String, String> refreshTokenCache = cacheManager.getCache(jwtProperties.getLogoutRefreshTokenName(), String.class, String.class);
            if (!cache.containsKey(token)) {
                return refreshTokenCache.containsKey(token);
            }
            return true;
        }
        return false;
    }
}
