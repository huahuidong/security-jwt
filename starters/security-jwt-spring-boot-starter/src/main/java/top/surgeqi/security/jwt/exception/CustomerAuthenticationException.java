package top.surgeqi.security.jwt.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;

/**
 * <p><em>Created by qipp on 2020/6/30 12:45</em></p>
 * 自定义公共认证异常类
 * <p>抛出自定义认证异常时</p>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class CustomerAuthenticationException extends AuthenticationException {

    public CustomerAuthenticationException(String msg, Throwable t) {
        super(msg, t);
        log.error("自定义认证异常 msg -> {}, {}", msg, t);
    }

    public CustomerAuthenticationException(String msg) {
        super(msg);
        log.error("自定义认证异常 msg -> {}", msg);
    }
}
