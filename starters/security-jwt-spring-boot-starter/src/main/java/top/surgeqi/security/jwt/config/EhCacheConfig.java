package top.surgeqi.security.jwt.config;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.core.EhcacheManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * <p><em>Created by qipp on 2020/10/6 10:23 上午</em></p>
 * EhCache 配置类
 * <p>
 * 当创建TokenCacheService类型的bean时此配置类将不会配置
 * {@code @ConditionalOnMissingBean(value = TokenCacheService.class)}
 * </p>
 * <blockquote><pre>
 * 名词解释
 *  preConfigured：缓存的名称，可以创建多个，可以对每个缓存进行不同的设置。
 *                 比方说对A缓存设置过期时间，B缓存不设置过期时间。
 *  String.class，String.class：你可以理解为是key是String类型的，value是String类型的。
 *                  等会获取缓存是根据String类型key来获取String类型的value的。
 *  heap(10000L)：代表preConfigured下的缓存最多存1000个key，如果是1001则把第一个缓存删除。
 *                  当然也可以设置offHeap()这个代表如果超过heap()设置的缓存数，
 *                  则把缓存存到offHeap()设置的磁盘上。（本次没用）
 *  withExpiry：对preConfigured下的缓存设置超时时间（上面的例子是20秒自动过期），
 *              EhCache的缓存过期机制有3种，想了解的可以去官网。
 *              <a href ="http://www.ehcache.org/">ehcache官网</a>
 * </pre></blockquote>
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
@Configuration
@ConditionalOnMissingBean(value = TokenCacheService.class)
public class EhCacheConfig {

    /**
     * JWT相关配置
     */
    private final JwtProperties jwtProperties;

    public EhCacheConfig(JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    @Bean
    public TokenCacheService tokenEhCacheServiceImpl() {
        return new TokenEhCacheServiceImpl(cacheManager(),jwtProperties);
    }

    /**
     * 配置缓存
     * @author qipp
     * @return org.ehcache.CacheManager
     */
    @Bean
    public EhcacheManager cacheManager() {
        log.info("使用ehcache缓存存储用户登出令牌");
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                // 令牌缓存
                .withCache(jwtProperties.getLogoutAccessTokenName(),
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(
                                String.class, String.class, ResourcePoolsBuilder.heap(10000L))
                                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
                                        Duration.ofMillis(jwtProperties.getAccessExpiration())))
                )
                // 刷新令牌缓存
                .withCache(jwtProperties.getLogoutRefreshTokenName(),
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(
                                String.class, String.class, ResourcePoolsBuilder.heap(10000L))
                                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
                                        Duration.ofMillis(jwtProperties.getRefreshExpiration())))
                )
                .build();
        cacheManager.init();
        return (EhcacheManager)cacheManager;
    }
}
