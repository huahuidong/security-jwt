package top.surgeqi.security.jwt.bean;

import lombok.Data;
import lombok.Getter;

/**
 * <p><em>Created by qipp on 2020/6/2 11:05</em></p>
 * 自定义令牌中的Subject
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class CustomerTokenSubject {

    /**
     * 用户名
     */
    private String username;


    /**
     * 令牌类型
     */
    private String tokenType;

    /**
     * <p><em>Created by qipp on 2020/6/2 11:05</em></p>
     * 令牌类型枚举
     *
     * @author <a href="https://gitee.com/qipengpai">qipp</a>
     * @since 1.0.1
     */
    @Getter
    public enum TokenType {

        /**
         * 令牌
         */
        ACCESS_TOKEN("access_token"),

        /**
         * 刷新令牌
         */
        REFRESH_TOKEN("refresh_token"),
        ;

        /**
         * 令牌类型
         */
        private final String type;

        TokenType(String type) {
            this.type = type;
        }
    }
}
