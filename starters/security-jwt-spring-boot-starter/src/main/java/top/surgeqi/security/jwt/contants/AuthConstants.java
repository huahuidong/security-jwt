package top.surgeqi.security.jwt.contants;

/**
 * <p><em>Created by qipp on 2020/6/30 12:43</em></p>
 * 认证相关常量
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public class AuthConstants {

    private AuthConstants() {
    }

    /**
     * 请求令牌类型{@value}
     */
    public static final String AUTH_REQ_TOKEN_TYPE = "Bearer ";

    /**
     * 令牌类型{@value}
     */
    public static final String AUTH_TOKEN_TYPE = "bearer";

    /**
     * 响应ContentType
     */
    public static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * 认证路径{@value}
     */
    public static final String LOGIN_URL = "/auth/**";

    /**
     * 认证模式{@value}
     */
    public static final String AUTH_TYPE = "authType";

    /**
     * 认证参数用户名/账号{@value}
     */
    public static final String AUTH_USERNAME = "username";

    /**
     * 认证参数密码{@value}
     */
    public static final String AUTH_PASSWORD = "password";

    /**
     * 认证参数刷新token {@value}
     */
    public static final String AUTH_REFRESH_TOKEN = "refreshToken";

}
