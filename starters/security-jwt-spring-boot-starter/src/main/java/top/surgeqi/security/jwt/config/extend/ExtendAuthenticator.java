package top.surgeqi.security.jwt.config.extend;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * <p><em>Created by qipp on 2020/6/30 10:41</em></p>
 * 扩展验证器接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public interface ExtendAuthenticator {


    /**
     * 处理扩展认证
     * @param entity 扩展认证实体
     * @return org.springframework.security.core.userdetails.UserDetails 用户账户详情
     * @author qipp
     */
    UserDetails authenticate(ExtendAuthenticationEntity entity);

    /**
     * 判断是否支持扩展认证类型
     * @param entity 扩展认证实体
     * @return boolean 是否支持扩展认证类型
     * @author qipp
     */
    boolean support(ExtendAuthenticationEntity entity);
}
