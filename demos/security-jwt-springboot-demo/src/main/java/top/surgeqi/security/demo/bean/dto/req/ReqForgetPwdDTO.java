package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p><em>Created by qipp on 2020/10/14 16:30</em></p>
 * 用户忘记密码 DTO
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "用户忘记密码")
public class ReqForgetPwdDTO {

    @NotNull(message = "手机号为空")
    @ApiModelProperty("手机号")
    private String phone;

    @NotNull(message = "手机验证码为空")
    @ApiModelProperty("手机验证码")
    private String phoneCode;

    @NotNull(message = "密码为空")
    @ApiModelProperty(value = "密码")
    private String password;
}
