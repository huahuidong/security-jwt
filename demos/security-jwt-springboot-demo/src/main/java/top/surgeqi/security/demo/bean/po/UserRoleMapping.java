package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 用户角色映射
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class UserRoleMapping {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 账户id
     */
    private Long accountId;

    /**
     * 角色id
     */
    private Long roleId;
}
