package top.surgeqi.security.demo.bean.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

/**
 * <p><em>Created by qipp on 2020/10/6 4:33 下午</em></p>
 * 用户账户信息 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "用户账户信息")
public class ResUserAccountDTO {

    @ApiModelProperty(value = "主键id")
    private String mid;

    @ApiModelProperty(value = "用户名/账号")
    private String username;

    @ApiModelProperty(value = "电话号")
    private String phone;

    @ApiModelProperty(value = " 是否启用 1:启用 0:未启用")
    private Boolean isEnabled;

    @ApiModelProperty(value = "是否锁定 0:锁定 1:未锁定")
    private Boolean isUnlocked;

    @ApiModelProperty(value = "是否过期 0：过期 1：未过期")
    private Boolean isUnexpired;

    @ApiModelProperty(value = "是否删除 0：未删除 1：删除")
    private Boolean isDelete;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "权限角色集合")
    private Collection<? extends GrantedAuthority> authorities;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "M:男 F:女 N:未知")
    private String sex;

    @ApiModelProperty(value = "个性签名")
    private String personalizedSignature;
}
