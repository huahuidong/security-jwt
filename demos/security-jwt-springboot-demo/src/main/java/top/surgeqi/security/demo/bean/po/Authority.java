package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 权限
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class Authority implements GrantedAuthority, Comparable<Authority> {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 键
     */
    private String name;

    /**
     * 值
     */
    private String value;

    /**
     * 组织机构
     */
    private String organize;

    /**
     * 拼接角色
     * <p>AUTH_ + {@code this.organize} + _ + {@code this.value}</p>
     * @return java.lang.String 完整权限名称
     * @author qipp
     */
    @Override
    public String getAuthority() {
        return "AUTH_" + this.organize +
                "_" +
                this.value;
    }

    /**
     * 判断相同的方法
     * @param o 对比角色对象
     * @return int 0、 1、 -1
     * @author qipp
     */
    @Override
    public int compareTo(Authority o) {
        //判断id是否相同
        if (this.mid.equals(o.getMid())) {
            return 0;
        }
        if(this.mid > o.getMid()){
            return 1;
        }
        return -1;
    }
}
