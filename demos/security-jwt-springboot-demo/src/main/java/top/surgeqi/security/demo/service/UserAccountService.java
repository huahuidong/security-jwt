package top.surgeqi.security.demo.service;

import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.ReqDeleteUserInfoDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqForgetPwdDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqUpdatePwdDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqUserAccountInitializeDTO;
import top.surgeqi.security.demo.bean.dto.res.ResUserAccountDTO;
import top.surgeqi.security.demo.bean.po.UserAccount;

/**
 * <p><em>Created by qipp on 2020/2/27 14:27</em></p>
 * 用户账户Service
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public interface UserAccountService {

    /**
     * 根据用户名或手机号查询用户账户
     *
     * @param username 用户名或手机号
     * @return org.surge.oauth2common.model.UserAccount
     * @author qipp
     */
    UserAccount selectByUserName(String username);

    /**
     * 根据手机号查询用户账户
     *
     * @param phone 手机号
     * @return org.surge.oauth2common.model.UserAccount
     * @author qipp
     */
    UserAccount selectByPhone(String phone);

    /**
     * 组合用户账户对象与角色权限
     *
     * @param userAccount 用户账户对象
     * @return org.surge.oauth2common.model.UserAccount
     * @author qipp
     */
    UserAccount composeUserDetailsAndAuthority(UserAccount userAccount);

    /**
     * 根据微信unionId查询用户
     *
     * @param unionId 微信Id
     * @return org.surge.oauth2common.model.UserAccount
     * @author qipp
     */
    UserAccount selectWeChatUnionId(String unionId);

    /**
     * 根据用户账户详情
     * @return org.surge.oauth2common.model.UserAccount
     * @author qipp
     */
    Result<ResUserAccountDTO> selectUserAccountInfo();

    /**
     * 用户注册
     * @author qipp
     * @param reqUserAccountInitializeDTO 用户名 昵称 手机号 验证码 密码
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    Result<Object> initializeUser(ReqUserAccountInitializeDTO reqUserAccountInitializeDTO);

    /**
     * 用户忘记密码
     * @author qipp
     * @param reqForgetPwdDTO 手机号 验证码 密码
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    Result<Object> forgetPwd(ReqForgetPwdDTO reqForgetPwdDTO);

    /**
     * 更新用户密码
     * @author qipp
     * @param reqForgetPwdDTO 密码 新密码
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    Result<Object> updatePwd(ReqUpdatePwdDTO reqForgetPwdDTO);

    /**
     * 删除用户
     * @author qipp
     * @param reqDeleteUserInfoDTO 用户账户ID
     * @return top.surgeqi.security.demo.bean.core.Result<java.lang.Object>
     */
    Result<Object> deleteUser(ReqDeleteUserInfoDTO reqDeleteUserInfoDTO);
}
