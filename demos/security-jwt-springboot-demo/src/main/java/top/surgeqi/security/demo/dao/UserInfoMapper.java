package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.UserInfo;

/**
 *  用户信息Mapper 接口
 *
 * @author qipp
 * @date 2020-05-15
 * @since 1.0.1
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
