package top.surgeqi.security.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import top.surgeqi.security.demo.bean.core.ReqPage;
import top.surgeqi.security.demo.bean.core.ResPage;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.ReqUpdateUserInfoDTO;
import top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO;
import top.surgeqi.security.demo.bean.po.UserAccount;
import top.surgeqi.security.demo.service.UserInfoService;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 *  用户信息控制器
 *
 * @author qipp
 * @date 2020-05-15
 * @since 1.0.1
 */
@Api(tags = {"用户信息相关"} )
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    /**
     * 用户信息服务类
     */
    private final UserInfoService userInfoService;

    public UserInfoController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    /**
     * 获取用户详情信息
     * @author qipp
     * @return top.surgeqi.security.demo.bean.result.Result<top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO>
     */
    @ApiOperation(value = "获取用户详情信息")
    @GetMapping
    public Result<ResGetUserInfoDTO> getUserInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserAccount userAccount = (UserAccount)authentication.getPrincipal();
        return userInfoService.getUserInfoDTO(userAccount.getMid());
    }

    /**
     * 用户更新信息
     * @author qipp
     * @param reqUpdateUserInfoDTO 用户更新信息 DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @PutMapping
    @ApiOperation(value = "用户更新信息")
    public Result<Object> forgetPwd(@Valid @RequestBody ReqUpdateUserInfoDTO reqUpdateUserInfoDTO) {
        return userInfoService.updateUserInfo(reqUpdateUserInfoDTO);
    }

    /**
     * 获取用户列表
     * @author qipp
     * @return top.surgeqi.security.demo.bean.result.Result<java.util.List<top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO>>
     */
    @ApiOperation(value = "获取用户列表")
    @PostMapping("/list")
    public Result<ResPage<ResGetUserInfoDTO,LocalDateTime>> getUserInfoList(@Valid @RequestBody ReqPage<ReqUpdateUserInfoDTO, LocalDateTime> reqUpdateUserInfoDTO){
        return userInfoService.getUserInfoList(reqUpdateUserInfoDTO);
    }
}
