package top.surgeqi.security.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

/**
 * <p><em>Created by qipp on 2020/5/29 11:28</em></p>
 *  spring security 验证配置
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ConfigurationProperties(SecurityProperties.PREFIX)
public class SecurityProperties {
    /**
     * 认证配置前缀{@value}
     */
    public static final String PREFIX = "surge";

    private Map<String, List<String>> antMatchers;

}
