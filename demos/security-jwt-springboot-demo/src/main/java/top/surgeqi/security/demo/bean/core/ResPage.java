package top.surgeqi.security.demo.bean.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p><em>Created by qipp on 2020/10/15 9:32 下午</em></p>
 * 分页响应结果
 * T 结果集范型
 * P 分页标识位
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "分页入参")
public class ResPage<T,P> {

    @ApiModelProperty("查询第几页")
    private Long page;

    @ApiModelProperty("每页条数")
    private Long rows;

    @ApiModelProperty("总条数")
    private Long total;

    @ApiModelProperty("分页标志位")
    private P flag;

    @ApiModelProperty("结果列表")
    private List<T> list;
}
