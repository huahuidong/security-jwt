package top.surgeqi.security.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * <p><em>Created by qipp on  2019/1/23 16:52</em></p>
 * spring 容器工具类
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
@Component
public class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    /**
     * 通过name获取 Bean
     * <p>
     *    不建议这样使用   
     * </p>
     * @param applicationContext 应用上下文
     * @author qipp
     */
    @Override
    public void setApplicationContext(@Nullable ApplicationContext applicationContext){
        if (SpringUtil.applicationContext == null) {
            SpringUtil.applicationContext = applicationContext;
        }
        log.info("-----------top.surgeqi.security.demo.util--");
        log.info("========ApplicationContext配置成功,在普通类可以通过调用SpringUtils.getAppContext()获取applicationContext对象,applicationContext=" + SpringUtil.applicationContext + "========");
    }

    /**
     * 获取applicationContext
     *
     * @return org.springframework.context.ApplicationContext
     * @author qipp
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 通过name获取 Bean
     *
     * @param name bean name
     * @return java.lang.Object bean
     * @author qipp
     */
    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    /**
     * 通过class获取Bean
     *
     * @param <T>   bean class类型
     * @param clazz class 对象
     * @return T bean
     * @author qipp
     */
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 通过name,以及Clazz返回指定的Bean
     *
     * @param <T>   bean class类型
     * @param name  bean name
     * @param clazz class 对象
     * @return T bean
     * @author qipp
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }

}
