# 亲爱的机器人


<a name="overview"></a>
## Overview
亲爱的机器人接口文档


### Version information
*Version* : 1.0.1


### URI scheme
*Host* : localhost:8611  
*BasePath* : /


### Tags

* completable-future-controller : Completable Future Controller
* excel-demo-controller : Excel Demo Controller
* 测试接口相关 : Test Controller
* 用户信息相关 : User Info Controller
* 用户账户相关 : User Account Controller




<a name="paths"></a>
## Paths

<a name="completablefuturetestusingget"></a>
### completableFutureTest
```
GET /completableFutureTest
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* completable-future-controller


<a name="exportusingget"></a>
### export
```
GET /export
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* excel-demo-controller


<a name="hellousingget"></a>
### 测试接口hello
```
GET /hello
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«UserAccount»](#5f21440c1471f8bf928bf02d65d0aaa5)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 测试接口相关


<a name="hellosuccessusingget"></a>
### 测试接口helloSuccess
```
GET /helloSuccess
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 测试接口相关


<a name="forgetpwdusingput"></a>
### 用户忘记密码
```
PUT /unAuthenticated/forgetPwd
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqForgetPwdDTO**  <br>*required*|reqForgetPwdDTO|[用户忘记密码](#e74d6365f9acfc8df8b3d0b5f1b24567)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="initializeuserusingpost"></a>
### 用户注册
```
POST /unAuthenticated/user
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqUserAccountInitializeDTO**  <br>*required*|reqUserAccountInitializeDTO|[用户注册](#e804a9a2158eb06e6609b0b7ac9c3cb9)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="updatepwdusingput"></a>
### 更新用户密码
```
PUT /updatePwd
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqForgetPwdDTO**  <br>*required*|reqForgetPwdDTO|[用户更新密码](#0546385d22a0003e926c72d808678da7)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="getuserusingget"></a>
### 获取用户账户信息
```
GET /user
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«用户账户信息»](#6c63e6a49133c506a9ca9012ece6269e)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="deleteuserusingdelete"></a>
### 删除用户
```
DELETE /user
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqDeleteUserInfoDTO**  <br>*required*|reqDeleteUserInfoDTO|[删除用户](#10991ce4202cd387bdf42385e506a13a)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="userlogoutusingpost"></a>
### 用户登出
```
POST /user/logout
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqUserLogoutDTO**  <br>*required*|reqUserLogoutDTO|[用户退出登录](#46f4484867a894d31f12f7e9a72550f8)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="user1usingget"></a>
### user1
```
GET /user1
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**name**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="user2usingget"></a>
### user2
```
GET /user2
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**authenticated**  <br>*optional*|boolean|
|**Query**|**authorities[0].authority**  <br>*optional*|string|
|**Query**|**credentials**  <br>*optional*|object|
|**Query**|**details**  <br>*optional*|object|
|**Query**|**principal**  <br>*optional*|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="user3usingget"></a>
### user3
```
GET /user3
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户账户相关


<a name="getuserinfousingget"></a>
### 获取用户详情信息
```
GET /userInfo
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«获取用户详情信息»](#096d505c035e20639acabac820884ef1)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `\*/*`


#### Tags

* 用户信息相关


<a name="forgetpwdusingput_1"></a>
### 用户更新信息
```
PUT /userInfo
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqUpdateUserInfoDTO**  <br>*required*|reqUpdateUserInfoDTO|[更新用户详情信息](#4d11a6ec5758bc6158aa8e1349369fe4)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«object»](#717e3925a11deabbeb28e6549b27297f)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户信息相关


<a name="getuserinfolistusingpost"></a>
### 获取用户列表
```
POST /userInfo/list
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**reqUpdateUserInfoDTO**  <br>*required*|reqUpdateUserInfoDTO|[分页入参«更新用户详情信息,LocalDateTime»](#74a887018e06b7b10162ac650d4b80f2)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[返回结果«分页入参«获取用户详情信息,LocalDateTime»»](#3e7c39e7c25c4fb9a7f0ce415aa720ed)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `\*/*`


#### Tags

* 用户信息相关




<a name="definitions"></a>
## Definitions

<a name="grantedauthority"></a>
### GrantedAuthority

|Name|Schema|
|---|---|
|**authority**  <br>*optional*|string|


<a name="2041141f690f06e701793e0d6613b2d1"></a>
### JSR303校验错误显示

|Name|Description|Schema|
|---|---|---|
|**errorMessage**  <br>*optional*|校验错误消息|string|
|**field**  <br>*optional*|校验错误字段|string|


<a name="useraccount"></a>
### UserAccount

|Name|Schema|
|---|---|
|**accountNonExpired**  <br>*optional*|boolean|
|**accountNonLocked**  <br>*optional*|boolean|
|**authorities**  <br>*optional*|< [GrantedAuthority](#grantedauthority) > array|
|**createTime**  <br>*optional*|string (date-time)|
|**credentialsNonExpired**  <br>*optional*|boolean|
|**enabled**  <br>*optional*|boolean|
|**isDelete**  <br>*optional*|boolean|
|**isEnabled**  <br>*optional*|boolean|
|**isUnexpired**  <br>*optional*|boolean|
|**isUnlocked**  <br>*optional*|boolean|
|**mid**  <br>*optional*|integer (int64)|
|**password**  <br>*optional*|string|
|**phone**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**username**  <br>*optional*|string|


<a name="74a887018e06b7b10162ac650d4b80f2"></a>
### 分页入参«更新用户详情信息,LocalDateTime»

|Name|Description|Schema|
|---|---|---|
|**flag**  <br>*optional*|分页标志位|string (date-time)|
|**page**  <br>*optional*|查询第几页|integer (int64)|
|**parameter**  <br>*optional*|分页参数|[更新用户详情信息](#4d11a6ec5758bc6158aa8e1349369fe4)|
|**rows**  <br>*optional*|每页条数|integer (int64)|


<a name="a408cd31865e7c9e003a8b2d6db2c221"></a>
### 分页入参«获取用户详情信息,LocalDateTime»

|Name|Description|Schema|
|---|---|---|
|**flag**  <br>*optional*|分页标志位|string (date-time)|
|**list**  <br>*optional*|结果列表|< [获取用户详情信息](#e275f4ccb11bde1baab9c70837a1091d) > array|
|**page**  <br>*optional*|查询第几页|integer (int64)|
|**rows**  <br>*optional*|每页条数|integer (int64)|
|**total**  <br>*optional*|总条数|integer (int64)|


<a name="10991ce4202cd387bdf42385e506a13a"></a>
### 删除用户

|Name|Description|Schema|
|---|---|---|
|**accountId**  <br>*optional*|用户账户Id|integer (int64)|
|**isDelete**  <br>*optional*|是否删除 0：未删除 1：删除|boolean|
|**isEnabled**  <br>*optional*|是否启用 1:启用 0:未启用|boolean|
|**isUnexpired**  <br>*optional*|是否过期 0：过期 1：未过期|boolean|
|**isUnlocked**  <br>*optional*|是否锁定 0:锁定 1:未锁定|boolean|


<a name="4d11a6ec5758bc6158aa8e1349369fe4"></a>
### 更新用户详情信息

|Name|Description|Schema|
|---|---|---|
|**accountId**  <br>*optional*|用户账户Id|integer (int64)|
|**addressCode**  <br>*optional*|地址|string|
|**birthday**  <br>*optional*|生日|string (date-time)|
|**email**  <br>*optional*|邮箱|string|
|**headPortrait**  <br>*optional*|头像|string|
|**id**  <br>*optional*|主键|integer (int64)|
|**isDelete**  <br>*optional*|是否删除 0：未删除 1：删除|boolean|
|**nickname**  <br>*optional*|昵称|string|
|**personalizedSignature**  <br>*optional*|个性签名|string|
|**qqNumber**  <br>*optional*|QQ号|string|
|**sex**  <br>*optional*|M:男 F:女 N:未知|string|
|**wechatNumber**  <br>*optional*|微信号|string|


<a name="e74d6365f9acfc8df8b3d0b5f1b24567"></a>
### 用户忘记密码

|Name|Description|Schema|
|---|---|---|
|**password**  <br>*optional*|密码|string|
|**phone**  <br>*optional*|手机号|string|
|**phoneCode**  <br>*optional*|手机验证码|string|


<a name="0546385d22a0003e926c72d808678da7"></a>
### 用户更新密码

|Name|Description|Schema|
|---|---|---|
|**newPassword**  <br>*optional*|新密码|string|
|**password**  <br>*optional*|密码|string|


<a name="e804a9a2158eb06e6609b0b7ac9c3cb9"></a>
### 用户注册

|Name|Description|Schema|
|---|---|---|
|**nickname**  <br>*optional*|昵称|string|
|**password**  <br>*optional*|密码|string|
|**phone**  <br>*optional*|手机号|string|
|**phoneCode**  <br>*optional*|手机验证码|string|
|**username**  <br>*optional*|用户唯一账号|string|


<a name="08d157f8245d3bd87e5b5f358313484d"></a>
### 用户账户信息

|Name|Description|Schema|
|---|---|---|
|**authorities**  <br>*optional*|权限角色集合|< [GrantedAuthority](#grantedauthority) > array|
|**createTime**  <br>*optional*|创建时间|string (date-time)|
|**headPortrait**  <br>*optional*|头像|string|
|**isDelete**  <br>*optional*|是否删除 0：未删除 1：删除|boolean|
|**isEnabled**  <br>*optional*|是否启用 1:启用 0:未启用|boolean|
|**isUnexpired**  <br>*optional*|是否过期 0：过期 1：未过期|boolean|
|**isUnlocked**  <br>*optional*|是否锁定 0:锁定 1:未锁定|boolean|
|**mid**  <br>*optional*|主键id|string|
|**nickname**  <br>*optional*|昵称|string|
|**personalizedSignature**  <br>*optional*|个性签名|string|
|**phone**  <br>*optional*|电话号|string|
|**sex**  <br>*optional*|M:男 F:女 N:未知|string|
|**username**  <br>*optional*|用户名/账号|string|


<a name="46f4484867a894d31f12f7e9a72550f8"></a>
### 用户退出登录

|Name|Description|Schema|
|---|---|---|
|**accessToken**  <br>*optional*|令牌|string|
|**refreshToken**  <br>*optional*|刷新令牌|string|


<a name="e275f4ccb11bde1baab9c70837a1091d"></a>
### 获取用户详情信息

|Name|Description|Schema|
|---|---|---|
|**accountId**  <br>*optional*|用户账户Id|integer (int64)|
|**addressCode**  <br>*optional*|地址|string|
|**birthday**  <br>*optional*|生日|string (date-time)|
|**createTime**  <br>*optional*|注册时间|string (date-time)|
|**email**  <br>*optional*|邮箱|string|
|**headPortrait**  <br>*optional*|头像|string|
|**id**  <br>*optional*|主键|integer (int64)|
|**isDelete**  <br>*optional*|是否已删除0:否 1:是|boolean|
|**nickname**  <br>*optional*|昵称|string|
|**personalizedSignature**  <br>*optional*|个性签名|string|
|**phone**  <br>*optional*|电话号|string|
|**qqNumber**  <br>*optional*|QQ号|string|
|**sex**  <br>*optional*|M:男 F:女 N:未知|string|
|**updateTime**  <br>*optional*|更新时间|string (date-time)|
|**username**  <br>*optional*|用户账号|string|
|**wechatNumber**  <br>*optional*|微信号|string|


<a name="5f21440c1471f8bf928bf02d65d0aaa5"></a>
### 返回结果«UserAccount»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|响应状态码|string|
|**data**  <br>*optional*|返回结果|[UserAccount](#useraccount)|
|**msg**  <br>*optional*|响应状态消息|string|
|**subCode**  <br>*optional*|业务响应状态消息|string|
|**subMsg**  <br>*optional*|业务响应状态消息|string|
|**success**  <br>*optional*||boolean|
|**verificationFailedMsgList**  <br>*optional*|响应状态消息|< [JSR303校验错误显示](#2041141f690f06e701793e0d6613b2d1) > array|


<a name="717e3925a11deabbeb28e6549b27297f"></a>
### 返回结果«object»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|响应状态码|string|
|**data**  <br>*optional*|返回结果|object|
|**msg**  <br>*optional*|响应状态消息|string|
|**subCode**  <br>*optional*|业务响应状态消息|string|
|**subMsg**  <br>*optional*|业务响应状态消息|string|
|**success**  <br>*optional*||boolean|
|**verificationFailedMsgList**  <br>*optional*|响应状态消息|< [JSR303校验错误显示](#2041141f690f06e701793e0d6613b2d1) > array|


<a name="3e7c39e7c25c4fb9a7f0ce415aa720ed"></a>
### 返回结果«分页入参«获取用户详情信息,LocalDateTime»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|响应状态码|string|
|**data**  <br>*optional*|返回结果|[分页入参«获取用户详情信息,LocalDateTime»](#a408cd31865e7c9e003a8b2d6db2c221)|
|**msg**  <br>*optional*|响应状态消息|string|
|**subCode**  <br>*optional*|业务响应状态消息|string|
|**subMsg**  <br>*optional*|业务响应状态消息|string|
|**success**  <br>*optional*||boolean|
|**verificationFailedMsgList**  <br>*optional*|响应状态消息|< [JSR303校验错误显示](#2041141f690f06e701793e0d6613b2d1) > array|


<a name="6c63e6a49133c506a9ca9012ece6269e"></a>
### 返回结果«用户账户信息»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|响应状态码|string|
|**data**  <br>*optional*|返回结果|[用户账户信息](#08d157f8245d3bd87e5b5f358313484d)|
|**msg**  <br>*optional*|响应状态消息|string|
|**subCode**  <br>*optional*|业务响应状态消息|string|
|**subMsg**  <br>*optional*|业务响应状态消息|string|
|**success**  <br>*optional*||boolean|
|**verificationFailedMsgList**  <br>*optional*|响应状态消息|< [JSR303校验错误显示](#2041141f690f06e701793e0d6613b2d1) > array|


<a name="096d505c035e20639acabac820884ef1"></a>
### 返回结果«获取用户详情信息»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|响应状态码|string|
|**data**  <br>*optional*|返回结果|[获取用户详情信息](#e275f4ccb11bde1baab9c70837a1091d)|
|**msg**  <br>*optional*|响应状态消息|string|
|**subCode**  <br>*optional*|业务响应状态消息|string|
|**subMsg**  <br>*optional*|业务响应状态消息|string|
|**success**  <br>*optional*||boolean|
|**verificationFailedMsgList**  <br>*optional*|响应状态消息|< [JSR303校验错误显示](#2041141f690f06e701793e0d6613b2d1) > array|





