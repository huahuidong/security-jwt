# IDEA中类的模板

>IDEA中在创建类时会自动给添加注释
## 模版内容

```plain
/**
 * <p><em>Created on ${DATE} ${TIME}</em></p>
 * 
 *
 * @author ${USER}
 * @since 1.0.1
 */
```
## 设置位置

### windows

*File-->settings-->Editor-->File and Code Templates-->Files*

### mac

*Preference-->Editor-->File and Code Templates-->Files*

## 设置简介

可对Class 、Interface、Enum 等进行设置

![图片](https://uploader.shimo.im/f/7ABoBe7jNpt6bnlz.png!thumbnail)


## **注释效果**

![图片](https://uploader.shimo.im/f/oPRIVdqKCx0ZOEkI.png!thumbnail)

# 
# IDEA中方法的模板

>IDEA中方法的模板方法的注释尤为重要，IDEA 自带生成注释只带有参数返回值，不够全面，所以需要手动设置模板，使用模板的方式可大大提高文档编写效率！
## 模版内容

```plain
*
 *
 $param$        
 * @return $return$
 * @throws $throws$
 */
```
## 设置位置

### windows

*File-->settings-->Editor-->L**i**v**e Templates*

### mac

*Preference-->Editor-->L**i**v**e Templates*

## 设置简介

1. 新建模版分组 （例如 user）
2. 点击新建模版
3. 设置新建模版缩写 （例如 * ）
4. 模版快捷键默认为Tab 也可自行
5. 设置模版介绍
6. 设置模版内容
7. 设置变量表达式
|return|methodReturnType()|
|:----|:----|
|throws|completeSmart()|
|param|groovyScript("def result=''; def params=\"${_1}\".replaceAll('[\\\\[\|\\\\]\|\\\\s]', '').split(',').toList(); for(i = 0; i < params.size(); i++) {result+='* @param ' + params[i] + ((i < params.size() - 1) ? '\\n ' : '')}; return result", methodParameters())|

***参考图例**

![图片](https://uploader.shimo.im/f/j72orbWPvdfSBBX0.png!thumbnail)

***注意创建时需要定义上下文为Java**

![图片](https://uploader.shimo.im/f/x7x4dHI7q0woebmn.png!thumbnail)

## **注释使用方式**

1. /*+ 模版缩写 + 快捷键
2. 生成模版后逐个属性点击enter / tab 确认
3. 对属性 、返回值、抛出异常进行描述
4. 完善方法用途及描述
## **注释效果**

![图片](https://uploader.shimo.im/f/JRT37O3WU7iIi1iC.png!thumbnail)

